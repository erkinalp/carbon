# Carbon Chat

Carbon is the Matrix client for Discord and Guilded refugees.

## The dream

Carbon's planned features, compared to Discord and Guilded:

- End to end encryption
- Free of charge per-account custom emojis and custom emoji packs
- No limit to number of groups you can join at a time
- Uses the open Matrix and Mumble systems
- Much better IRC layout
- Probably more

Carbon's planned features, compared to Element:

- Get rid of the unmanageable combined rooms list
- Groups like Discord/Guilded guilds
  - Always have one group selected at a time
  - Synchronised membership, moderators, power levels and bans
  - Ordered channel list
  - Unread indicators
- Add existing channels to groups
- Pin any channel to the groups bar
- Tidy Mumble integration to add voice channels
- More reliable unreads
- Per-account custom emojis (Ponies+FluffyChat integration) and custom emoji packs
- Slightly better IRC layout
- Probably more

## The reality

Carbon is currently _technically_ usable as a chat app, but is very
early in development. These important features still need to be
implemented:

- Login GUI
- Unreads
- Chat history
- Formatting
- Emojis
- Reactions
- Groups v2
- Group management
- Pinned channels
- Mumble integration

## The code

### Building

    npm install -D
    npm run rebuild

### Hosting

Send the files from the `build` folder to a static file server. Apply
a long cache-control header to everything served under `/static`, and
no cache-control header to everything else.

### Developing

    npm run watch

Files will be rebuilt as you save them.
