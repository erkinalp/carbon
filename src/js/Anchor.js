import {ElemJS} from $to_relative "/js/basic.js"

class Anchor extends ElemJS {
	constructor() {
		super("div")
		this.class("c-anchor")
	}

	scroll() {
		// console.log("anchor scrolled")
		this.element.scrollIntoView({block: "start"})
	}
}

export {Anchor}
