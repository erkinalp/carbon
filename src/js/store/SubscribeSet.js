import {Subscribable} from $to_relative "/js/store/Subscribable.js"

class SubscribeSet extends Subscribable {
	constructor() {
		super()
		Object.assign(this.events, {
			addItem: [],
			deleteItem: [],
			changeItem: [],
			askAdd: []
		})
		Object.assign(this.eventDeps, {
			addItem: ["changeItem"],
			deleteItem: ["changeItem"],
			changeItem: [],
			askAdd: []
		})
		this.set = new Set()
	}

	has(key) {
		return this.set.has(key)
	}

	forEach(f) {
		for (const key of this.set.keys()) {
			f(key)
		}
	}

	askAdd(key) {
		this.broadcast("askAdd", key)
	}

	add(key) {
		if (!this.set.has(key)) {
			this.set.add(key)
			this.broadcast("addItem", key)
		}
	}

	delete(key) {
		if (this.set.has(key)) {
			this.set.delete(key)
			this.broadcast("deleteItem", key)
		}
	}
}

export {SubscribeSet}
