import {Subscribable} from $to_relative "/js/store/Subscribable.js"
import {SubscribeValue} from $to_relative "/js/store/SubscribeValue.js"

class SubscribeMap extends Subscribable {
	constructor() {
		super()
		Object.assign(this.events, {
			addItem: [],
			changeItem: [],
			removeItem: []
		})
		this.map = new Map()
	}

	has(key) {
		return this.map.has(key) && this.map.get(key).exists()
	}

	get(key) {
		if (this.map.has(key)) {
			return this.map.get(key)
		} else {
			this.map.set(key, new SubscribeValue())
		}
	}

	set(key, value) {
		let s
		if (this.map.has(key)) {
			s = this.map.get(key).set(value)
			this.broadcast("changeItem", key)
		} else {
			s = new SubscribeValue().set(value)
			this.map.set(key, s)
			this.broadcast("addItem", key)
		}
		return s
	}
}

export {SubscribeMap}
