import {Subscribable} from $to_relative "/js/store/Subscribable.js"
import {SubscribeMapList} from $to_relative "/js/store/SubscribeMapList.js"
import {SubscribeSet} from $to_relative "/js/store/SubscribeSet.js"
import {SubscribeValue} from $to_relative "/js/store/SubscribeValue.js"

const store = {
	groups: new SubscribeMapList(SubscribeValue),
	rooms: new SubscribeMapList(SubscribeValue),
	directs: new SubscribeSet(),
	activeGroup: new SubscribeValue(),
	activeRoom: new SubscribeValue(),
	newEvents: new Subscribable()
}

window.store = store

export {store}
