import * as lsm from $to_relative "/js/lsm.js"

function resolveMxc(url, size, method) {
	const [server, id] = url.match(/^mxc:\/\/([^/]+)\/(.*)/).slice(1)
	if (size && method) {
		return `${lsm.get("domain")}/_matrix/media/r0/thumbnail/${server}/${id}?width=${size}&height=${size}&method=${method}`
	} else {
		return `${lsm.get("domain")}/_matrix/media/r0/download/${server}/${id}`
	}
}

export {resolveMxc}
