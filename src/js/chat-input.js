import {q} from $to_relative "/js/basic.js"
import {store} from $to_relative "/js/store/store.js"
import * as lsm from $to_relative "/js/lsm.js"
import {chat} from $to_relative "/js/chat.js"

const input = q("#c-chat-textarea")

store.activeRoom.subscribe("changeSelf", () => {
	if (store.activeRoom.exists()) {
		input.focus()
	}
})

input.addEventListener("keydown", event => {
	if (event.key === "Enter" && !event.shiftKey && !event.ctrlKey) {
		event.preventDefault()
		const body = input.value
		send(input.value)
		input.value = ""
		fixHeight()
	}
})

input.addEventListener("input", () => {
	fixHeight()
})

function fixHeight() {
	input.style.height = "0px"
	// console.log(input.clientHeight, input.scrollHeight)
	input.style.height = (input.scrollHeight + 1) + "px"
}

function send(body) {
	if (!store.activeRoom.exists()) return
	return store.activeRoom.value().timeline.send(body)
}
