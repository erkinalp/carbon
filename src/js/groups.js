import {q} from $to_relative "/js/basic.js"

let state = "CLOSED"

const groups = q("#c-groups-display")
const rooms = q("#c-rooms")

groups.addEventListener("click", () => {
	groups.classList.add("c-groups__display--closed")
})

rooms.addEventListener("mouseout", () => {
	groups.classList.remove("c-groups__display--closed")
})
