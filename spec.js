module.exports = [
	{
		type: "file",
		source: "/assets/fonts/whitney-500.woff",
		target: "/static/whitney-500.woff"
	},
	{
		type: "file",
		source: "/assets/fonts/whitney-400.woff",
		target: "/static/whitney-400.woff"
	},
	{
		type: "js",
		source: "/js/basic.js",
		target: "/static/basic.js"
	},
	{
		type: "js",
		source: "/js/groups.js",
		target: "/static/groups.js"
	},
	{
		type: "js",
		source: "/js/chat-input.js",
		target: "/static/chat-input.js"
	},
	{
		type: "js",
		source: "/js/room-picker.js",
		target: "/static/room-picker.js"
	},
	{
		type: "js",
		source: "/js/store/store.js",
		target: "/static/store/store.js"
	},
	{
		type: "js",
		source: "/js/store/Subscribable.js",
		target: "/static/store/Subscribable.js"
	},
	{
		type: "js",
		source: "/js/store/SubscribeValue.js",
		target: "/static/store/SubscribeValue.js"
	},
	{
		type: "js",
		source: "/js/store/SubscribeMapList.js",
		target: "/static/store/SubscribeMapList.js"
	},
	{
		type: "js",
		source: "/js/store/SubscribeSet.js",
		target: "/static/store/SubscribeSet.js"
	},
	{
		type: "js",
		source: "/js/sync/sync.js",
		target: "/static/sync/sync.js"
	},
	{
		type: "js",
		source: "/js/lsm.js",
		target: "/static/lsm.js"
	},
	{
		type: "js",
		source: "/js/Timeline.js",
		target: "/static/Timeline.js"
	},
	{
		type: "js",
		source: "/js/Anchor.js",
		target: "/static/Anchor.js"
	},
	{
		type: "js",
		source: "/js/chat.js",
		target: "/static/chat.js"
	},
	{
		type: "js",
		source: "/js/functions.js",
		target: "/static/functions.js"
	},
	{
		type: "file",
		source: "/assets/fonts/whitney-500.woff",
		target: "/static/whitney-500.woff"
	},
	{
		type: "file",
		source: "/assets/icons/directs.svg",
		target: "/static/directs.svg"
	},
	{
		type: "file",
		source: "/assets/icons/channels.svg",
		target: "/static/channels.svg"
	},
	{
		type: "file",
		source: "/assets/icons/join-event.svg",
		target: "/static/join-event.svg"
	},
	{
		type: "sass",
		source: "/sass/main.sass",
		target: "/static/main.css"
	},
	{
		type: "pug",
		source: "/home.pug",
		target: "/index.html"
	},
	{
		type: "pug",
		source: "/login.pug",
		target: "/login.html"
	}
]
